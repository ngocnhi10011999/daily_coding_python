'''
    Given an array of integers, find the first missing positive integer
    in linear time and constant space. In other words, find the lowest
    positive integer that does not exist in the array.
    The array can contain duplicates and negative numbers as well.
'''
# print('Enter element of list')
# x = input()
# print('------------------------------------------')
# list_input = [int(e) for e in x.split()]
# print('input = ', list_input)
# 
# result = [1]
# for e in list_input:
#     if e + 1 not in list_input and e + 1 > 0:
#         result.append(e + 1)
#         break
#     else:
#         continue
# 
# print("negative numbers :", min(result))

def smallest_positive(list_input):
    result = []
    for e in list_input:
        if e + 1 not in list_input and e + 1 > 0:
            result.append(e + 1)
        else:
            continue

    return min(result)

assert 1 == smallest_positive([0])
assert 2 == smallest_positive([1,4,3,6,5])
assert 2 == smallest_positive([1,44,3,66,55])
assert 6 == smallest_positive([1,2,3,4,5])
assert 4 == smallest_positive([-6, 3, 10, 14, 17, 6, 14, 1, -5, -8, 8, 15, 17, -10, 2, 7, 11, 2, 7, 11])
assert 4 == smallest_positive([18, 2, 13, 3, 3, 0, 14, 1, 18, 12, 6, -1, -3, 15, 11, 13, -8, 7, -8, -7])
assert 4 == smallest_positive([-6, 3, 10, 14, 17, 6, 14, 1, -5, -8, 8, 15, 17, -10, 2, 7, 11, 2, 7, 11])
assert 3 == smallest_positive([7, -7, 19, 6, -3, -6, 1, -8, -1, 19, -8, 2, 4, 19, 5, 6, 6, 18, 8, 17])
