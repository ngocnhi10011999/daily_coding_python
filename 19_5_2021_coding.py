'''
    Given the mapping a = 1, b = 2, ... z = 26, and an encoded message,
    count the number of ways it can be decoded.
'''
import string

alphabet = list(string.ascii_lowercase)
number = [num for num in range(1, 27)]
dict_map = dict(zip(number, alphabet))

num_check = 111

def check(num):
    word = dict_map.get(num)
    if word:
        return True
    else:
        return False

def totalDigitsOfNumber(n):
    total = 0
    while (n > 0):
        total += 1
        n = int(n / 10)
    return total

# vaible_num = []
# if num_check % 10 > 0:
#     num = num_check % 10
#     vaible_num.append(num)
# if 

print(totalDigitsOfNumber(1234589759823752093458203948029))
