'''
    * Given a list of numbers and a number k, return whether
    any two numbers from the list add up to k.

    * For example, given [10, 15, 3, 7] and k of 17,
    return true since 10 + 7 is 17.
'''

def check_list(list_need, k):
    result = []
    list_copy = list_need.copy()
    for i, e in enumerate(list_need):
        if e > k: continue
        else:
            list_copy.pop(0)
            if (k-e) in list_copy:
                result.append((e, k-e))
            else: continue
            list_copy.append(e)
    if result:
        show_result = f"Two numbers has sum equal {k} is {result[0]} and {result[1]}"
    else:
        show_result = "Has no two numbers in list has sum equal k"
    print(show_result)


def __init__():
    print("Enter emlement of list")
    x = input()
    print("-----------------------------------------")
    list_need = [int(e) for e in x.split()]
    # return list_need
    print("list = ", list_need)
    print("-----------------------------------------")
    print("-----------------------------------------")
    print("Enter a number need check")
    print("-----------------------------------------")
    k = input()
    k = int(k)
    if len(list_need) < 2:
        print("Lenght of list great than 2")
        __init__()
    else:
        check_list(list_need, k)

__init__()
