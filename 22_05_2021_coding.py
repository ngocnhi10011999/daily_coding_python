'''
    Implement a job scheduler which takes in a function f and an integer n,
    and calls f after n milliseconds.
'''

import time

def scheduler(f,n):
    time.sleep(0.001*n)
    return f

print(scheduler('abc', 10000))
