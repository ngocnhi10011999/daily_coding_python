'''
    * Given an array of integers, return a new array such that
    each element at index i of the new array is the product of
    all the numbers in the original array except the one at i.

    * For example, if our input was [1, 2, 3, 4, 5], the expected
    output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1],
    the expected output would be [2, 3, 6]
'''
print('Enter element of list')
x = input()
print('------------------------------------------')
list_input = [int(e) for e in x.split()]
print('input = ', list_input)
multiply = 1
multiply_list = []
for i, e in enumerate(list_input):
    multiply *= e

for e in list_input:
    multiply_list.append(int(multiply / e))

print('output = ', multiply_list)

print('------------------------------------------')
print("* Follow-up: what if you can't use division?")
multiply_ = 1
multiply_list_ = []
print('input = ', list_input)

list_copy = list_input.copy()

for i, e in enumerate(list_input):
    list_copy.pop(0)
    for k in list_copy:
        multiply_ *= k
    list_copy.append(e)
    multiply_list_.append(multiply_)
    multiply_ = 1

print('output = ', multiply_list_)
