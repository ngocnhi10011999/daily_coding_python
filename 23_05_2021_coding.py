'''
 Implement an autocomplete system. 
 That is, given a query string s and a set of all possible query strings, 
 return all strings in the set that have s as a prefix.
'''
def complete_system(list_input: list, str_check: str):
    result = []

    for e in list_input:
        index = len(str_check)
        if str_check == e[:index]:
            result.append(e)
    
    return result

assert ['deer', 'deal'] == complete_system(['dog', 'deer', 'deal'], 'de')
assert ['apple', 'apply'] == complete_system(['banana', 'apple', 'apply', 'laptop'], 'ap')
